# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)


            
User.create!(name:  "Jane Foster",
             email: "jane@gmail.com",
             password:              "foobar",
             password_confirmation: "foobar",
             activated: true,
             activated_at: Time.current)
             
User.create!(name:  "Drew Goddyn",
             email: "drew@test.com",
             password:              "testtest",
             password_confirmation: "testtest",
             admin: true,
             activated: true,
             activated_at: Time.current)
             
User.create!(name:  "Ivy Olamit",
             email: "ivy@test.com",
             password:              "testtest",
             password_confirmation: "testtest",
             activated: true,
             activated_at: Time.current)
             
User.find_by(email: 'ivy@test.com').microposts.create(content: "Man do I ever love costco sundaes!")

User.create!(name:  "Jeff Huang",
             email: "jeffh@test.com",
             password:              "testtest",
             password_confirmation: "testtest",
             activated: true,
             activated_at: Time.current)
             
User.find_by(email: 'jeffh@test.com').microposts.create(content: "Where the blizzards at?!")
             
User.create!(name:  "Brian O'Reilly",
             email: "brian@test.com",
             password:              "testtest",
             password_confirmation: "testtest",
             activated: true,
             activated_at: Time.current)

User.find_by(email: 'brian@test.com').microposts.create(content: "I AM the Irish Dolphin")

User.create!(name:  "Nathan Mots",
             email: "nathan@test.com",
             password:              "testtest",
             password_confirmation: "testtest",
             activated: true,
             activated_at: Time.current)
             
User.find_by(email: 'nathan@test.com').microposts.create(content: "DJ Jazzy Nate reporting for duty")

User.create!(name:  "Steve Thompson",
             email: "steve@test.com",
             password:              "testtest",
             password_confirmation: "testtest",
             activated: true,
             activated_at: Time.current)
             
User.find_by(email: 'steve@test.com').microposts.create(content: "sooooo when's the calgary office opening?")

User.create!(name:  "Fernando Witzke",
             email: "fernando@test.com",
             password:              "testtest",
             password_confirmation: "testtest",
             activated: true,
             activated_at: Time.current)
             
User.find_by(email: 'fernando@test.com').microposts.create(content: "Can everyone start calling me Fernie please?")

User.create!(name:  "Mike Cramm",
             email: "mike@test.com",
             password:              "testtest",
             password_confirmation: "testtest",
             activated: true,
             activated_at: Time.current)
             
User.find_by(email: 'mike@test.com').microposts.create(content: "I got nothing... my name is Mike Cramm")

User.create!(name:  "Anna Romanova",
             email: "anna@test.com",
             password:              "testtest",
             password_confirmation: "testtest",
             activated: true,
             activated_at: Time.current)
             
User.find_by(email: 'anna@test.com').microposts.create(content: "Where the hell are our integration tests?")
             
User.create!(name:  "Jeff Marvin",
             email: "jeffm@test.com",
             password:              "testtest",
             password_confirmation: "testtest",
             activated: true,
             activated_at: Time.current)
             
User.find_by(email: 'jeffm@test.com').microposts.create(content: "You know what I wish I had? more babies")
             
User.create!(name:  "John Brennan",
             email: "john@test.com",
             password:              "testtest",
             password_confirmation: "testtest",
             activated: true,
             activated_at: Time.current)
             
User.find_by(email: 'john@test.com').microposts.create(content: "We should switch to React.js")
             
User.create!(name:  "Aaron cheng",
             email: "aaron@test.com",
             password:              "testtest",
             password_confirmation: "testtest",
             activated: true,
             activated_at: Time.current)
             
User.find_by(email: 'aaron@test.com').microposts.create(content: "Hey bro, can I get in on that fidget spinner?")

User.create!(name:  "Alec Clarke",
             email: "alec@test.com",
             password:              "testtest",
             password_confirmation: "testtest",
             activated: true,
             activated_at: Time.current)
             
User.find_by(email: 'alec@test.com').microposts.create(content: "Baby cheese in the house!")
             
User.create!(name:  "Wanxing Wang",
             email: "wanxing@test.com",
             password:              "testtest",
             password_confirmation: "testtest",
             activated: true,
             activated_at: Time.current)
             
User.find_by(email: 'wanxing@test.com').microposts.create(content: "you guys are weird, this company is weird...I NEED AN ADULT!")
             
User.create!(name:  "Michael Cheung",
             email: "michael@test.com",
             password:              "testtest",
             password_confirmation: "testtest",
             activated: true,
             activated_at: Time.current)
             
User.find_by(email: 'michael@test.com').microposts.create(content: "so everyone's done the clio learns milestones right?")
             
User.create!(name:  "Irina Belova",
             email: "irina@test.com",
             password:              "testtest",
             password_confirmation: "testtest",
             activated: true,
             activated_at: Time.current)
             
User.find_by(email: 'irina@test.com').microposts.create(content: "Put some more shrimp on the bawby")
             
User.create!(name:  "Steven Harbaruk",
             email: "steven@test.com",
             password:              "testtest",
             password_confirmation: "testtest",
             activated: true,
             activated_at: Time.current)
             
User.find_by(email: 'steven@test.com').microposts.create(content: "GET BACK TO WORK YOU HOSERS")


#following relationships
users = User.all
users.each do |user|
  users.each do |other_user|
    unless other_user == user
      user.follow(other_user)
    end
  end
end