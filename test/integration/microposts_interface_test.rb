require 'test_helper'

class MicropostsInterfaceTest < ActionDispatch::IntegrationTest
  
  def setup
    @user = users(:michael)
  end
  
  test "micropost interface" do
    log_in_as(@user)
    get root_url
    assert_template '_logged_in_home'
    assert_select 'div.pagination'
    assert_select 'input[type=file]'
    #create an invalid post
    assert_no_difference 'Micropost.count' do
      post microposts_path, params: { micropost: { content: " " } }
    end
    assert_template 'static_pages/_logged_in_home'
    assert_select 'div#error_explanation'
    #create a valid post
    content = "this is a micropost"
    picture = fixture_file_upload('test/fixtures/rails.png', 'image/png')
    assert_difference 'Micropost.count' do
      post microposts_path, params: { micropost: { content: content, picture: picture } }
    end
    assert assigns(:micropost).picture?
    assert_redirected_to root_url
    follow_redirect!
    assert_match content, response.body
    #delete post
    first_post = @user.microposts.first
    assert_select 'a[href=?]', micropost_path(first_post), text: 'Delete'
    assert_difference 'Micropost.count', -1 do
      delete micropost_path(first_post)
    end
    assert_no_match content, response.body
    #navigate to another user page and ensure no delete links
    get user_path(users(:lana))
    assert_select 'a', text: 'Delete', count: 0
    
  end
  test 'micropost sidebar count' do
    log_in_as(@user)
    get root_url
    assert_match "34 microposts", response.body
    # user with no microposts
    log_in_as(users(:malory))
    get root_url
    assert_match "0 microposts", response.body
    users(:malory).microposts.create!(content:"meowmix is the tastiest")
    get root_url
    assert_match "1 micropost", response.body
  end
end
