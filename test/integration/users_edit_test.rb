require 'test_helper'

class UsersEditTest < ActionDispatch::IntegrationTest
  def setup
    @user = users(:michael)
  end
  
  test "unsuccesful user edit" do
    log_in_as(@user)
    get edit_user_path(@user)
    assert_template 'users/edit'
    patch user_path, params: { user:{ name: " ",
                              email: "derp@derp",
                              password: "foo",
                              password_confirmation: "bar" } }
    assert_template 'users/edit'
    assert_select 'div.alert', 'The form contains 4 errors.'
    assert_select 'div.field_with_errors'
  end
  
  test "successful user edit with friendly forwarding" do
    get edit_user_path(@user)
    log_in_as(@user)
    assert_redirected_to edit_user_url(@user)
    assert_nil session[:forwarding_url]
    name = "Michelle"
    email = "derp@gmail.com"
    patch user_path(@user), params: { user: { name: name,
                                       email: email,
                                       password: "",
                                       password_confirmation: "" } }
    assert_redirected_to @user
    assert_not flash.empty?
    @user.reload
    assert_equal name, @user.name
    assert_equal email, @user.email
  end
    
end
