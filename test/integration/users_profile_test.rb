require 'test_helper'

class UsersProfileTest < ActionDispatch::IntegrationTest
  include ApplicationHelper

  def setup
    @user = users(:michael)
  end
  
  test "profile display" do
    get user_path(@user)
    assert_template 'users/show'
    assert_select 'title', full_title(@user.name)
    assert_select '.user_info', @user.name
    assert_select '.user_info img.gravatar'
    assert_select '.col-md-8>h3', "Microposts (#{@user.microposts.count})"
    assert_select 'div.pagination', count: 1
    @user.microposts.paginate(page: 1).each do |post|
      assert_match post.content, response.body
    end
  end

end
