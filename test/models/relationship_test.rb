require 'test_helper'

class RelationshipTest < ActiveSupport::TestCase
  
  def setup
    @relationship = Relationship.new(followed_id: users(:michael).id, follower_id: users(:michael).id)
  end

  test "should be valid" do
    assert @relationship.valid?
  end
  
  test "follower_id should be required" do
    @relationship.follower_id = nil
    assert_not @relationship.valid?
  end
  
  test "followed_id should be required" do
    @relationship.followed_id = nil
    assert_not @relationship.valid?
  end
  
end