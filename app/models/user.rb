class User < ApplicationRecord
  has_many :microposts, dependent: :destroy
  # has_many x_relationships returns rows from the relationship table where the follower/followed_id = user.id
  # whereas has_many through: returns the user objects themselves such that:
  # User.where(id: user.active_relationships.first.followed_id) == user.following (assuming only 1 record) or
  #user.active_relationships.map{|x|User.where(id: x.followed_id)} == user.following
  has_many :active_relationships,  class_name:  "Relationship",
                                   foreign_key: "follower_id",
                                   dependent:   :destroy
  has_many :passive_relationships, class_name:  "Relationship",
                                  foreign_key: "followed_id",
                                  dependent:   :destroy
  
  has_many :following, through: :active_relationships,  source: :followed
  has_many :followers, through: :passive_relationships, source: :follower
                                  
                                  
  attr_accessor :remember_token, :activation_token, :reset_token
  before_save :downcase_email
  before_create :create_activation_digest
  validates :name,  presence: true, length: { maximum: 50 }
  VALID_EMAIL_REGEX = /\A([\w+\-].?)+@[a-z\d\-]+(\.[a-z]+)*\.[a-z]+\z/i
  validates :email, presence:   true, 
                    length:     { maximum: 255 }, 
                    format:     { with: VALID_EMAIL_REGEX, message: "not a valid email" },
                    uniqueness: { case_sensitive: false }
  has_secure_password
  validates :password,  presence: true, length: { minimum: 6 }, allow_nil: true
  
  # Returns the hash digest of the given string.
  def User.digest(string)
    cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
                                                  BCrypt::Engine.cost
    BCrypt::Password.create(string, cost: cost)
  end

  # Returns a random token.
  def User.new_token
    SecureRandom.urlsafe_base64
  end

  # Remembers a user in the database for use in persistent sessions(cookies)
  def remember
    self.remember_token = User.new_token
    update_attribute(:remember_digest, User.digest(remember_token))
  end

  # Returns true if the given token matches the digest(also false if digest is nil)
  def authenticated?(attribute, token)
    digest = self.send("#{attribute}_digest")
    return false if digest.nil?
    BCrypt::Password.new(digest).is_password?(token)
  end

  # Forgets a user.
  def forget
    update_attribute(:remember_digest, nil)
  end
  
  #activate a signup user
  def activate
    self.update_columns(activated: true, activated_at: Time.current)
  end
  
  # send activation email to user
  def send_activation_email
    UserMailer.account_activation(self).deliver_now
  end
  
  def create_reset_digest
    self.reset_token = User.new_token
    update_columns(reset_digest:  User.digest(reset_token), reset_sent_at: Time.current)
  end
  
  def send_reset_email
    UserMailer.password_reset(self).deliver_now
  end
  
  def password_reset_expired?
    self.reset_sent_at < 2.hours.ago
  end
  
  def feed
    following_ids = "SELECT followed_id FROM relationships
                     WHERE  follower_id = :user_id"
    Micropost.where("user_id IN (#{following_ids})
                     OR user_id = :user_id", user_id: id)
  end
  
  def following?(other_user)
    self.following.include?(other_user)
  end
  
  def follow(other_user)
    self.following << other_user
  end
  
  def unfollow(other_user)
    self.following.delete(other_user)
    
  end
  
  private
  #converts email to all lower-case
  def downcase_email
    self.email.downcase!
  end
  
  #create activation token and digest
  def create_activation_digest
    self.activation_token = User.new_token
    self.activation_digest = User.digest(activation_token)
  end
  
end