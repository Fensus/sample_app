class SessionsController < ApplicationController
  def new
  end
  
  def create
    # find user based on the login form params
   @user = User.find_by(email: params[:session][:email].downcase)
   # if a user is found and the password is correct then log them in
    if @user && @user.authenticate(params[:session][:password])
      if @user.activated?
        log_in @user
        #remember user if they check the box
        remember(@user) if params[:session][:remember_me] == '1'
        redirect_back_or(root_url)
      else
        flash[:warning] = "Account not activated. Check your email for activation link"
        redirect_to root_url
      end
    else
      #don't log user in and reload the login page with error message
      flash.now[:danger] = "Invalid username and password, please try again"
      render 'new'
    end
  end
  
  def destroy
    log_out if logged_in?
    redirect_to root_path
  end
end
